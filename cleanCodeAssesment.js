class Book {
  constructor(title, author, publicationYear) {
    this.title = title;
    this.author = author;
    this.publicationYear = publicationYear;
  }

  updateDetails(title, author, publicationYear) {
    this.title = title;
    this.author = author;
    this.publicationYear = publicationYear;
  }

  getDetails() {
    return `Title: ${this.title}, Author: ${this.author}, Publication Year: ${this.publicationYear}`;
  }
}

class Library {
  constructor() {
    this.books = [];
  }

  addBook(book) {
    this.books.push(book);
  }

  searchByTitle(title) {
    return this.books.filter(
      (book) => book.title.toLowerCase() === title.toLowerCase()
    );
  }

  searchByAuthor(author) {
    return this.books.filter(
      (book) => book.author.toLowerCase() === author.toLowerCase()
    );
  }

  getTotalBooks() {
    return this.books.length;
  }
}

// Create book instances
const book1 = new Book("The Richest Man in Babylon", "George S. Clason", 1926);
const book2 = new Book("Deep Work", "Cal Newport", 2016);

// Create a library instance
const library = new Library();

// Add books to the library
library.addBook(book1);
library.addBook(book2);

// Search for books
const booksByAuthor = library.searchByAuthor("George S. Clason");
console.log("Books by author:", booksByAuthor);

const booksByTitle = library.searchByTitle("Deep Work");
console.log("Books by title:", booksByTitle);

// Display total number of books
console.log("Total books in the library:", library.getTotalBooks());